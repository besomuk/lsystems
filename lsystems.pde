//String myAxiom = "F+F+F+F";
//String myRule  = "F+F-F-FF+F+F-F";

// snowflake, ugao 60, gens 4, scale 10
//String myAxiom = "F++F++F";
//String myRule  = "F-F++F-F";

// Koch curve ( ugao 90 ), npr, generacija 5, scale 4
 //String myAxiom = "+F";
 //String myRule  = "F+F-F-F+F";

// Sierpinski triangle
//String myAxiom = "F";
//String myRule  = "G+F+G";

// DRVO, ugao 22, stavi draw tree na true
String myAxiom = "X";
String myRule  = "FF";
boolean drawTree = true;

//String myAxiom = "F+F+F+F";
//String myRule  = "FF+F-F+F+FF";

boolean live = false; // da li je ukljucena draw ()

int myGens  = 5;
int scale   = 10; // ovo je zapravo duzina jedne crtice

float rotationAngle = radians ( -30.5 );

String s; // string koji sadrzi sistem

void setup ()
{
  size ( 1900, 1000 );
  //noFill();
  stroke ( 255, 255, 255 );
  strokeWeight ( 1 ) ;
  
  println ( "Calculating sentence...");
  s = MakeSentence( myAxiom, myRule, myGens );
  println ( "Sentence done...");
  println ( "-----------------------" );
  println ( s );
  println ( "-----------------------" );
  println ( "Drawing...");  
  
  if ( drawTree )
    DrawSystemTree ( s );
   else
     DrawSystem( s );
    
  
  println ( "Drawing done...");
  
  //popMatrix();
  //pushMatrix();
  
  rotate ( 45 );
}

void draw ()
{
  if ( live )
  {
  //translate ( mouseX - width / 2, mouseY - height / 2);    
  //print ( mouseX - width / 2 );
  //myGens = int ( map ( mouseX, 100, 900, 0, 6) );
  
  //rotationAngle = map ( mouseX, 0, 1900, 10, 30);
  //println ( rotationAngle );
  

    if ( drawTree)
    DrawSystemTree ( s );
   else
     DrawSystem( s );
  }
}

/* ******************************************************* */

void DrawSystem( String s )
{
  background ( 0 );
  
  translate ( width / 2, height / 2 + 200);
  //translate ( width / myGens , height / ( myGens / 2f ) );
  
  circle ( 0, 0, 5); 
  
  int colorR = 0;
  int colorG = 128;
  int colorB = 255;
  
  char[] ca = s.toCharArray();
  for ( int i = 0; i < ca.length; i ++ ) // da probam kada krece od jedan. Kad je na pocetku promena ugla, nesto me zeza, to treba obraditi
  {     
    if ( ca[i] == 'F' )
    {
      line ( 0, 0, scale, 0 );
      //line ( 0, 0, 0, scale );
      translate ( scale, 0 );
    }
    if ( ca[i] == 'G' )
    {
      line ( 0, 0, scale, 0 ); // u slucaju Serpinski trougla, dajem mu da crta i kada je G
      translate ( scale, 0 );
    }       
    if ( ca[i] == 'X' )
    {
      line ( 0, 0, scale, 0 ); // u slucaju Serpinski trougla, dajem mu da crta i kada je G
      //line ( 0, 0, 0, scale );
      translate ( scale, 0 );
    }      
    if ( ca[i] == '[' )
    {
      pushMatrix();
    }
    if ( ca[i] == ']' )
    {
      popMatrix();
    }       
    
    if ( ca[i] == '+' )
    {
      rotate ( -rotationAngle );
    }
    
    if ( ca[i] == '-' )
    {
      rotate ( rotationAngle );
    }    
    
    /* Deo za menjanje boje */
    /*
    stroke ( colorR, colorG, colorB );    
    colorR ++;
    colorG ++;
    colorB ++;    
    if ( colorR >= 255 ) colorR = 0;
    if ( colorG >= 255 ) colorG = 0;
    if ( colorB >= 255 ) colorB = 0;
    */
  }  
  
  
}

/* Za drvo imam posebnu metodu, jer hocu da mi prva linija ide uvek i iskljucivo pravo uvis.
   Takodje, njegova pozicija je fiksna 
   */
   
void DrawSystemTree( String s )
{
  background ( 0 );
  
  translate ( width / 2, height / 2 + 400);
  //translate ( width / myGens , height / ( myGens / 2f ) );
  
  circle ( 0, 0, 5); 
  
  int colorR = 0;
  int colorG = 128;
  int colorB = 255;
  
  if ( scale > 0 )
    scale *= -1;
  
  char[] ca = s.toCharArray();
  for ( int i = 0; i < ca.length; i ++ ) // da probam kada krece od jedan. Kad je na pocetku promena ugla, nesto me zeza, to treba obraditi
  {     
    if ( ca[i] == 'F' )
    {
      stroke ( 255, 255, 255 );
      line ( 0, 0, 0, scale );
      translate ( 0, scale );
    }
    if ( ca[i] == 'G' )
    {
      line ( 0, 0, scale, 0 ); // u slucaju Serpinski trougla, dajem mu da crta i kada je G
      translate ( scale, 0 );
    }       
    if ( ca[i] == 'X' )
    {
      line ( 0, 0, 0, scale );
      translate ( 0, scale );
    }      
    if ( ca[i] == '[' )
    {
      float g = random (50, 255);
      fill ( 0, (int)g, 0 );
      stroke ( 0, (int)g, 0 );
      circle ( 0, 0, random ( 2, 10) );
      pushMatrix();      
    }
    if ( ca[i] == ']' )
    {
      float g = random (50, 255);
      fill ( 0, (int)g, 0 );
      stroke ( 0, (int)g, 0 );
      circle ( 0, scale*2, random ( 2, 10) );
       popMatrix();      
    }       
    
    if ( ca[i] == '+' )
    {
      rotate ( -rotationAngle );
      //rotate ( -rotationAngle + random ( -.1, .1 ));
    }
    
    if ( ca[i] == '-' )
    {
      rotate ( rotationAngle );
      //rotate ( rotationAngle + random ( -.1, .1 ));
    }    
    
    /* Deo za menjanje boje */
    /*
    stroke ( colorR, colorG, colorB );    
    colorR ++;
    colorG ++;
    colorB ++;    
    if ( colorR >= 255 ) colorR = 0;
    if ( colorG >= 255 ) colorG = 0;
    if ( colorB >= 255 ) colorB = 0;
    */
  }  
  
  
}

void DrawElement ()
{
  
}

String MakeSentence ( String axiom, String rule, int generations )
{
  if ( generations == 0 )
    return axiom;
    
  String result = "";
  
  // prolaz kroz generacije
  for ( int x = 0; x < generations; x++)
  {
    String temp = ""; 
    
    char[] ca = axiom.toCharArray();
    for ( int i = 0; i < ca.length; i ++ )
    {
      if ( ca[i] == 'F' )
      {
        temp += rule;
      }
      else if ( ca[i] == 'G' ) // paznja! ovo vazi samo za Sierpinski trougao!
      {
        temp += "F-G-F";
      }
      else if ( ca[i] == 'X' ) // paznja! ovo vazi samo za Sierpinski trougao!
      {
        //temp += "F-[[X]+X]+F[+FX]-X"; // original
        temp += "F-[[X]+X]+F[+FX]-X";
      }      
      else
      {
        temp += str ( ca[i] ); 
      }      
    }
    result = temp;
    axiom = result;
  }
  
  String original = "F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F-F+F-F-FF+F+F-FF+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F-F+F-F-FF+F+F-FF+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F-F+F-F-FF+F+F-FF+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F-F+F-F-FF+F+F-FF+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F";
  
  if ( original == result )
    println("BRAVO!");
  
  return result;
  // return "F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F-F+F-F-FF+F+F-FF+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F-F+F-F-FF+F+F-FF+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F-F+F-F-FF+F+F-FF+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F-F+F-F-FF+F+F-FF+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F-F+F-F-FF+F+F-F";
  //return "F+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F+F+F-F-FF+F+F-F";
}
